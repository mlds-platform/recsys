import time
from typing import Callable

from loguru import logger


def log_running_time(func: Callable) -> Callable:

    def with_time(*args, **kwargs):
        t_start = time.time()
        ret = func(*args, **kwargs)
        t_end = time.time()
        elapsed = t_end - t_start
        logger.info(f"Running {func.__name__} took {elapsed} seconds")
        return ret

    return with_time
