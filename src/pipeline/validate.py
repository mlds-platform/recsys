import great_expectations as gx

from src.utils.decorators import log_running_time

context = gx.get_context()


@log_running_time
def validate_data():

    validator = context.sources.pandas_default.read_csv("iris.csv")

    # validator.expect_column_values_to_not_be_null("pickup_datetime")
    validator.expect_column_values_to_be_between(
        "sepal length (cm)", min_value=0, max_value=10
    )
    validator.expect_column_values_to_be_between(
        "sepal width (cm)", min_value=0, max_value=10
    )
    validator.expect_column_values_to_be_between(
        "petal length (cm)", min_value=0, max_value=10
    )
    validator.expect_column_values_to_be_between(
        "petal width (cm)", min_value=0, max_value=10
    )
    validator.save_expectation_suite(discard_failed_expectations=False)

    checkpoint = context.add_or_update_checkpoint(
        name="my_quickstart_checkpoint",
        validator=validator,
    )

    checkpoint_result = checkpoint.run()

    context.build_data_docs()
    context.open_data_docs()

if __name__ == "__main__":
    validate_data()
