import json
import pickle

from dotenv import load_dotenv
import mlflow

load_dotenv()


def log_result():
    model = pickle.load(open("model.pkl", "rb"))
    metrics = json.load(open("accuracy.json", "r"))

    mlflow.set_experiment("Test Experiment")

    with mlflow.start_run():
        mlflow.log_metrics(metrics)
        mlflow.sklearn.log_model(
            model, artifact_path="model.pkl", registered_model_name="classifier"
        )
