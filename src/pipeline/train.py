import json
import pickle

from loguru import logger
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB

from src.pipeline.load import load_corpus
from src.pipeline.validate import validate_data
from src.utils.decorators import log_running_time


logger.info("loading data")
load_corpus()
logger.info("validating data")
validate_data()


@log_running_time
def train_model():

    iris = pd.read_csv("iris.csv")

    X = iris.iloc[:, :-1]
    Y = iris["y"]

    X_train, X_test, y_train, y_test = train_test_split(
        X, Y, test_size=0.4, random_state=42
    )

    logger.info("training model")
    model = GaussianNB()
    model.fit(X_train, y_train)

    preds = model.predict(X_test)
    acc = accuracy_score(preds, y_test)

    with open("accuracy.json", "w") as f:
        json.dump({"accuracy": acc}, f)

    with open("model.pkl", "wb") as f:
        pickle.dump(model, f)
