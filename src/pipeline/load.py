from sklearn import datasets
import pandas as pd

from src.utils.decorators import log_running_time


@log_running_time
def load_corpus():
    iris = datasets.load_iris()
    dataset = pd.DataFrame(iris.data, columns=iris.feature_names)
    dataset["y"] = iris.target
    dataset.to_csv("iris.csv", index=False)


if __name__ == "__main__":
    load_corpus()
