from src.pipeline.train import train_model
from src.pipeline.evaluate import log_result


if __name__ == "__main__":
    train_model()
    log_result()
