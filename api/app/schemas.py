from pydantic import BaseModel, Field


class ClientRequest(BaseModel):
    text: str = Field(
        ...,
        title="Запрос пользователя",
        description="Текст запроса пользователя",
        example="Пример запроса",
    )


class Prediction(BaseModel):
    score: float = Field(..., title="Прогноз", description="Ответ модели", example=0.5)
