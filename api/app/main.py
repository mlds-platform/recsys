from contextlib import asynccontextmanager
import json

import asyncio
from aiokafka import AIOKafkaProducer
from aiokafka.errors import KafkaConnectionError
from fastapi import FastAPI
from fastapi.middleware.gzip import GZipMiddleware
from loguru import logger
from starlette.middleware.cors import CORSMiddleware

from .schemas import ClientRequest, Prediction
from .config import app_config as Config


loop = asyncio.get_event_loop()
producer = AIOKafkaProducer(
    loop=loop, client_id=Config.PROJECT_NAME, bootstrap_servers=Config.KAFKA_URL
)


@asynccontextmanager
async def lifespan(app: FastAPI):
    try:
        await producer.start()
        logger.info("Kafka producer start")
        yield
        await producer.stop()
        logger.info("Kafka producer stop")
    except KafkaConnectionError:
        producer._closed = True
        logger.info("Start app without Kafka")
        yield
        logger.info("Stop app")


app = FastAPI(
    title=Config.PROJECT_NAME,
    description=f"Сервис для построения прогноза. Стенд: {Config.APP_ENV}",
    lifespan=lifespan,
)
app.add_middleware(GZipMiddleware, minimum_size=1000)
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def get_predictions(text: str) -> float:
    return 0.5


@app.post("/predict", response_model=Prediction)
async def predict(data: ClientRequest):

    score = get_predictions(data.text)

    # сохранение логов в Kafka
    log = data.dict()
    log["score"] = score
    if not producer._closed:
        asyncio.gather(
            producer.send(Config.TOPIC_NAME, json.dumps(log).encode("ascii"))
        )

    return Prediction(**{"score": score})
