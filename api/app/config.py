from pydantic import field_validator, ValidationInfo
from pydantic_settings import BaseSettings


class AppConfig(BaseSettings):
    APP_ENV: str
    PROJECT_NAME: str
    KAFKA_HOST: str
    KAFKA_PORT: str
    TOPIC_NAME: str
    KAFKA_URL: str = ""

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
        case_sensitive = True

    @field_validator("KAFKA_URL", mode="before")
    def set_kafka_url(cls, v: int, info: ValidationInfo):
        return info.data["KAFKA_HOST"] + ":" + info.data["KAFKA_PORT"]


app_config = AppConfig()
